# pages pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/pages?branch=main)](https://gitlab.com/buildgarden/pipelines/pages/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/pages)](https://gitlab.com/buildgarden/pipelines/pages/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Publish static sites to GitLab Pages.

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

This pipeline publishes the contents of a `public` directory to [GitLab
Pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works) on the
default branch. This ensures that documentation is always being built, but only
published when desired.

View the example site: https://buildgarden.gitlab.io/pipelines/pages/

## Usage

Include the pipeline:

```yaml
include:
  - project: buildgarden/pipelines/pages
    file: pages.yml
```

### Publish anything

You can publish anything you like by placing it in the `public` directory and
saving as an artifact:

```yaml
stages:
  - test
  - build
  - deploy

include:
  - project: buildgarden/pipelines/pages
    file: pages.yml

totally-custom-job:
  stage: build
  image: ${CONTAINER_PROXY}alpine
  script:
    - mkdir public
    - touch public/index.html
  artifacts:
    paths:
      - public/
```
